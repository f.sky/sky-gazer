# Sky-Gazer
This project is developed for tracking aircraft's activities retrieved from opensky-network.org


## Getting Started
The code is developed and tested in Python 3.6
There are 2 main parts, the retrieval of flights data from opensky-network API and the data visualization.


### Prerequisites
* Python 3.6
* ICAO24 id of your targeted aircraft.

```ICAO24 is the 24-bit identifier of the aircraft concerned. It is assigned by the International Civil Aviation Organisation, and is one of a block of addresses assigned to the same entity, usually a country.```

### Installing
No installation required.

## Running the scripts
* For acquaring flights
$ python flights_retrieval.py ICAO24
* For visualizing flights
$ python visualize_flights.py 

## Contributors
* Feel free to add your name here.

## Authors

* **Fawkes Skywalker** - Due to some safety reasons (although the flight tracking is just a pur curiosity), we prefer to remain anonymous.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgements

* Inspiration from AChaiyasitdhi in his blog (in thai language) https://thaistudentsoverseas.wordpress.com/2019/01/31/his-majesty-where-are-you/
* Not so thankful to COVID-19, but it is the reason why we have more time to work at home.
