import sys

from src import flights_retrieval
from src import visualize_flights
import config 

if __name__ == '__main__':
    # specify icao24 of a flight
    icao24 = config.ICAO24
    if len(sys.argv)>1:
        icao24 = sys.argv[1]
        
    # get all flights for the aircraft
    #flights_retrieval.main(icao24)
    # visualize the flights
    visualize_flights.main()