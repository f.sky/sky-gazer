import datetime

API_ENDPOINT = "https://opensky-network.org/api/flights/aircraft"

# must be an ICAO for a flight
ICAO24 = '8821ab'

FLIGHT_JSON_FILENAME = 'data/flights.json'
# identify the starting point for retrieving the history
START_DATE = datetime.datetime(2017, 1, 1, 0, 0, 0)

# should be retrieved online 
AIRPORT_CODE = {'EDDM':'Munich',
                'LSZH':'Zurich',
                'EDDN':'Nuremberg',
                'EDDL':'Düsseldorf',
                'EDDB':'Berlin-Schönefeld',
                'VTBD':'Don Mueang',
                'EDLW':'Dortmund',
                'OMDB':'Dubai',
                'LSME':'Emmen, CH',
                'EDVE':'Braunschweig',
                'EDDV':'Hannover',
                'EHWO':'Woensdrecht, NL',
                'EDJA':'Memmingen',
                'LIPY':'Marche, IT',
                'EGLF':'Farnborough, GB',
                'LOWS':'Salzburg',
                'LIEO':'Geasar, IT',
                'ENBR':'Bergen, NO',
                'LGKO':'Kos, GR',
                'EDDH':'Hamburg',
                'EDLE':'Essen',
                'VTBT':'Bang Pra',
                'EDAH':'Heringsdorf'}