# This code is inspired by AChaiyasitdhi
# https://thaistudentsoverseas.wordpress.com/2019/01/31/his-majesty-where-are-you/

import requests
import json 
import time
import datetime
import config 

def get_unix_time(dt):
    return int(time.mktime(dt.timetuple()))

def get_flights(icao24, begin, end):
    parameters = {'icao24':icao24,
                    'begin':begin,
                    'end':end}
    headers = {'accept':'application/json'}

    response = requests.get(config.API_ENDPOINT, 
                            params = parameters,
                            headers = headers)
    if response.content:
        return json.loads(response.content)
    else:
        return {}

    
    
def main(icao24):
        
    start = config.START_DATE
    end = datetime.datetime.now()

    # to use the API, end-begin cannot be > 30 days
    # so we split the query per 29 days
    delta = datetime.timedelta(days = 29) 
    pointer = start
    flights = []
    while pointer <= end :
        temp_end = pointer + delta
        #print(pointer, temp_end)
        flights += get_flights(icao24,
                        get_unix_time(pointer), 
                        get_unix_time(temp_end))
        pointer = temp_end
    
    # format json response
    formatted_resp = json.dumps(flights, indent=4)
    # save to a file
    with open(config.FLIGHT_JSON_FILENAME,'w') as f:
        f.write(formatted_resp)
        
    # TODO: get flight's positioning on board    