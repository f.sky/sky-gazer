import json 
import time
import datetime
import sys
import pandas as pd
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import config

def format_unix_time(unix_time):
    dt = datetime.datetime.utcfromtimestamp(unix_time)
    #print(dt.strftime('%Y-%m-%d'))
    #print(datetime.utcfromtimestamp(unix_time).strftime('%Y-%m-%d %H:%M:%S'))
    return dt

def count_visited_airports(df):
    dep_port = df.groupby([df['estDepartureAirport']]).size().reset_index(name='Count_d')
    dep_port = dep_port.rename(columns={'estDepartureAirport':'airport'})
    
    arr_port = df.groupby([df['estArrivalAirport']]).size().reset_index(name='Count_a')
    arr_port = arr_port.rename(columns={'estArrivalAirport':'airport'})
    
    airports = pd.merge(dep_port,arr_port, on='airport', how='outer').fillna(value=0)
    airports['Total'] = airports.apply(lambda x: int(x['Count_d']+x['Count_a']), axis=1)
    airports['airport_name'] = airports['airport'] .apply(lambda x: config.AIRPORT_CODE[x])
    return airports
    
def sort_dict_by_key(dict):
    for key in sorted(dict.keys()):
        print("%s: %s" % (key, dict[key]))

def plot_visited_airports(df):
    all_airports = count_visited_airports(df)
    x=all_airports['airport_name'].values
    y=all_airports['Total'].values
    plt.stem(x, y)
    # show value y on plot
    for i,j in zip(x,y):
        plt.text(i,j, str(j))
    plt.xticks(rotation=45)
    plt.title('Visited Airports since 2017')
    plt.show()

def plot_number_flights(df):
    df['dt'] = df['firstSeen'].apply(datetime.datetime.utcfromtimestamp)
    df['year'] = df['dt'].apply(lambda x: x.year)
    df['month'] = df['dt'].apply(lambda x: x.month)
    df['year-month'] = df['dt'].apply(lambda x: str(x.year) +'/'+ x.strftime('%m'))
    grouped = df.groupby([df['year-month']]).size().reset_index(name='Count')
    x = grouped['year-month'].values
    y = grouped['Count'].values
    plt.stem(x, y)
    # show value y on plot
    for i,j in zip(x,y):
        plt.text(i,j, str(j))
    plt.xticks(rotation=45)
    plt.title('Number of Flights since 2017')
    plt.show()
        
def plot_flight_duration(df):
    df['dt1'] = df['firstSeen'].apply(datetime.datetime.utcfromtimestamp)
    df['dt2'] = df['lastSeen'].apply(datetime.datetime.utcfromtimestamp)
    df['delta_sec'] = df.apply(lambda x: (x['dt2']-x['dt1']).seconds,axis=1)
    df['delta_hour'] = df['delta_sec'].apply(lambda x: x/3600)
    mean = round(df['delta_hour'].mean(),2)
    std = round(df['delta_hour'].std(),2)
    
    plt.hist(df['delta_hour'].values,30)
    plt.xlabel('Time on board (hour)')
    plt.ylabel('Number of Flights')
    plt.text(5, 50,'mean={}, std={}'.format(mean,std))
    plt.grid = True
    plt.show()

def plot_flight_per_weekday(df):
    df['weekday'] = df['firstSeen'].apply(lambda x: datetime.datetime.utcfromtimestamp(x).weekday())
    
    grouped = df.groupby([df['weekday']]).size().reset_index(name='Count')
    x = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']#grouped['weekday'].values
    y = grouped['Count'].values
    plt.stem(x, y)
    plt.title('Number of Flights per Weekday since 2017')
    plt.show()
    
def plot_routes(df):
    # TODO: plot on a world map with lat,long data
    flights_fw = df[['estDepartureAirport','firstSeen']]
    flights_fw = flights_fw.rename(columns={'estDepartureAirport':'airport','firstSeen':'time'})
    flights_bw = df[['estArrivalAirport','lastSeen']]
    flights_bw = flights_bw.rename(columns={'estArrivalAirport':'airport','lastSeen':'time'})
    flights = flights_fw
    flights = flights.append(flights_bw)
    flights = flights.sort_values(by=['time'])
    flights['time'] = flights['time'].apply(format_unix_time)
    flights['airport'] = flights['airport'].apply(lambda x: config.AIRPORT_CODE[x] if x != None else 'undefined')
    
    plt.xticks(rotation=45)
    plt.xlabel('Time of travel')
    plt.ylabel('Airport')
    plt.plot('time','airport', data=flights, marker='o', color='mediumvioletred')
    plt.show()
    
def main():
    # load raw data
    df = pd.read_json (config.FLIGHT_JSON_FILENAME)
    
    # plot the count of visited airports 
    plot_visited_airports(df)
    
    # plot a frequency of flights per month
    plot_number_flights(df)
            
    # plot the duration (time) per a flight
    plot_flight_duration(df)
    
    # plot a frequency of flights vs. weekdays)
    plot_flight_per_weekday(df)
    
    # plot depart airport and arrival route vs. timeline
    # TODO: update the plot to the map
    plot_routes(df)